import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Checkbox from 'material-ui/Checkbox'
import Paper from 'material-ui/Paper'
import {List, ListItem} from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import axios from 'axios'

const modalWidth = {
  maxWidth: 'none',
}

class ModalDialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
        open: false,
        tempItem: [],
        tempModel: [],
        tempType: [],
      }
  }

  handleOpen = () => {
    Promise.all([
      axios.get('https://api.myjson.com/bins/7y3fr'),
      axios.get('https://api.myjson.com/bins/8jj1j'),
      axios.get('https://api.myjson.com/bins/ga5wn')
    ])
      .then(response => {
        let itemType = response[0].data;
        let itemModel = response[1].data;
        let item = response[2].data;
        if (this.props.filter) {
          itemType = itemType.filter(vehicleType => !!this.props.filter.find(filterTypes => vehicleType.id === filterTypes.vehicleTypeId));
          itemModel = itemModel.filter(vehicleModel => !!this.props.filter.find(filterModels => vehicleModel.id === filterModels.vehicleModelId));
          item = item.filter(vehicle => !!this.props.filter.find(filterItems => vehicle.id === filterItems.vehicleId));
        }

        this.setState({
          tempType: itemType.map(elem => {
            elem.checked = false
            return elem }),
          tempModel: itemModel.map(elem => {
            elem.checked = false
            return elem }),
          tempItem: item.map(elem => {
            elem.checked = false
            return elem }),
        })
      })
      .catch(error => {
        console.error(error)
      })
    this.setState({open: true})
  }

  handleClose = () => {
    this.setState({
      open: false,
      tempType: [],
      tempModel: [],
      tempItem: [],
    })
  }

  sortCheckboxes = (ind, type) => {
    let tempType = this.state.tempType;
    let tempModel = this.state.tempModel;
    let tempItem = this.state.tempItem;

    if ( type === "itemType" ) {
      tempType[ind].checked = !tempType[ind].checked
    }
    if ( type === "itemModel" ) {
      tempModel[ind].checked = !tempModel[ind].checked
    }
    if ( type === "item" ) {
      tempItem[ind].checked = !tempItem[ind].checked
    }

    this.setState({
      tempType: this.state.tempType,
      tempModel: this.state.tempModel,
      tempItem: this.state.tempItem,
    })
  }

  getChecked = () => {
    let tempType = this.state.tempType;
    let tempModel = this.state.tempModel;
    let tempItem = this.state.tempItem;
    console.log(
      tempType.filter(key => !!key.checked).map(key => key.name),
      tempModel.filter(key => !!key.checked).map(key => key.name),
      tempItem.filter(key => !!key.checked).map(key => key.name),
    )
    this.handleClose()
  }

  render() {
    const actions = [
      <FlatButton
        label="Скасувати"
        primary={true}
        onClick={this.handleClose}
      />,
      <RaisedButton
        primary={true}
        label="Підтвердити"
        onClick={this.getChecked}
      />,
    ]

    return (
      <div>
        <RaisedButton
          className="primary-margin"
          label={this.props.title}
          primary={true}
          onClick={this.handleOpen}
        />

        <Dialog
          title="Vehicles"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentStyle={modalWidth}
          titleClassName="center-align"
          autoScrollBodyContent={true}
        >
          <div className="flex-block">
            <Paper zDepth={1} className="flex-item">
              <List>
                <Subheader>Vehicle Type</Subheader>
                {this.state.tempType.map((key, ind) => {
                  return (
                    <ListItem
                      key={ind}
                      primaryText={key.name}
                      leftCheckbox={
                        <Checkbox
                          key={ind}
                          id={key.id}
                          checked={key.checked}
                          onCheck={() => this.sortCheckboxes(ind, 'itemType')}
                        />
                      }
                    />
                  )
                })}
              </List>
            </Paper>

            <Paper zDepth={1} className="flex-item">
              <List>
                <Subheader>Vehicle Model</Subheader>
                {this.state.tempModel.map((key, ind) => {
                  return (
                    <ListItem
                      key={ind}
                      primaryText={key.name}
                      leftCheckbox={
                        <Checkbox
                          key={ind}
                          id={key.id}
                          checked={key.checked}
                          onCheck={() => this.sortCheckboxes(ind, 'itemModel')}
                        />
                      }
                    />
                  )
                })}
              </List>
            </Paper>

            <Paper zDepth={1} className="flex-item">
              <List>
                <Subheader>Vehicle</Subheader>
                {this.state.tempItem.map((key, ind) => {
                  return (
                    <ListItem
                      key={ind}
                      primaryText={key.name}
                      leftCheckbox={
                        <Checkbox
                          key={ind}
                          id={key.id}
                          checked={key.checked}
                          onCheck={() => this.sortCheckboxes(ind, 'item')}
                        />
                      }
                    />
                  )
                })}
              </List>
            </Paper>
          </div>
        </Dialog>
      </div>
    )
  }
}

export default ModalDialog
