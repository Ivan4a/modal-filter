import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ModalDialog from './components/ModalDialog'

const filterArray = [
  { vehicleTypeId: 1 },
  { vehicleTypeId: 2 },
  { vehicleModelId: 1 },
  { vehicleId: 1 }
]

const App = () => (
  <MuiThemeProvider>
    <div>
      <ModalDialog filter={filterArray} title="With filter" />
      <ModalDialog title="Without filter" />
    </div>
  </MuiThemeProvider>
)

export default App
